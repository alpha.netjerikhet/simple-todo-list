const form = document.querySelector('form');
const btnAdd = document.querySelector('#btn-add');
const item = document.querySelector('#item');
const ulList = document.querySelector('.ul-list');
const DATAKEY = 'storedData';
const ERRORKEY = 'errorMsg';
const btnReset = document.querySelector('#btn-reset');
const errorMessage = document.querySelector('#error-msg');
const btnTrash = document.querySelector('.trash-btn');
let storedItems = localStorage.getItem(DATAKEY) === null ? [] : JSON.parse(localStorage.getItem(DATAKEY));
let errorText = localStorage.getItem(ERRORKEY) === null ? '' : JSON.parse(localStorage.getItem(ERRORKEY));

const createItem = (e) => {
    e.preventDefault();
    if (item.value !== '') {
        localStorage.removeItem(ERRORKEY);
        errorMessage.textContent = '';
        const liItem = document.createElement('li');
        liItem.innerHTML = `${item.value}<a class="trash-btn" href="#"><i class="fa fa-lg fa-trash" aria-hidden="true"></i></a>`;
        console.log(item);
        ulList.append(liItem);
        item.value = '';
        storedItems.push(liItem.textContent);
        localStorage.setItem(DATAKEY, JSON.stringify(storedItems));
    } else {
        // error msg
        errorText = 'Error. Please add an item!';
        localStorage.setItem(ERRORKEY, JSON.stringify(errorText));
        errorMessage.innerHTML = `<i class="fa fa-exclamation-triangle"></i> ${errorText}`;
    }

};

const clearLocalStorage = () => {
    localStorage.removeItem(DATAKEY);
    localStorage.removeItem(ERRORKEY);
    storedItems = [];
    errorMessage.textContent = '';
};

const removeItem = (e) => {
    storedItems = JSON.parse(localStorage.getItem(DATAKEY));
    if (e.target.classList.contains('fa-trash')) {
        e.target.parentNode.parentNode.remove();
        storedItems.forEach((item, index) => {
            if (item === e.target.parentNode.parentNode.textContent) {
                storedItems.splice(index, 1);
                errorMessage.textContent = '';
            }
        });
        storedItems.length > 0 ? localStorage.setItem(DATAKEY, JSON.stringify(storedItems)) : clearLocalStorage();
    }
};

btnAdd.addEventListener('click', createItem);

ulList.addEventListener('click', removeItem);

btnReset.addEventListener('click', clearLocalStorage);

storedItems.forEach(item => {
    const liItem = document.createElement('li');
    liItem.innerHTML = `${item}<a class="trash-btn" href="#"><i class="fa fa-lg fa-trash" aria-hidden="true"></i></a>`;
    ulList.append(liItem);
    errorMessage.textContent = errorText;
});