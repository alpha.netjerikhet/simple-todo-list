**Simple ToDo List**

This is a simple application to practice basic JavaScript concepts learned recently.

*Features*
- [x] [Live Server](https://github.com/ritwickdey/vscode-live-server) is used to serve the page.
- [x] Application contains an input field and add button to add items to the todo list.
- [x] There is a reset button to clear all items.
- [x] If input field is blank then it will not be added and error message is displayed.
- [x] localStorage is used to store data.
- [x] Remove button to remove each item separately.
- [x] Refining the UI